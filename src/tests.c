#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"

#include "tests.h"
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>

#define SEPARATOR_LENGTH 40
#define REGION_MIN_SIZE (2 * 4096)
#define SMALL_BLOCK_SIZE 512
#define MEDIUM_BLOCK_SIZE 1024
#define DOUBLE_MEDIUM_BLOCK_SIZE (1024 * 2)
#define BIG_BLOCK_SIZE_1 10000
#define BIG_BLOCK_SIZE_2 16000

enum TEST_RESULT common_allocate_test() {
    void* heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stderr, heap);
    void* allocated_memory = _malloc(SMALL_BLOCK_SIZE);
    debug_heap(stderr, heap);
    if (!allocated_memory) return BAD_ALLOCATE_ONE_BLOCK;
    _free(allocated_memory);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    return OK;
}

enum TEST_RESULT one_block_free_test() {
    void* heap = heap_init(REGION_MIN_SIZE);

    debug_heap(stderr, heap);
    void* first_block = _malloc(MEDIUM_BLOCK_SIZE);
    void* second_block = _malloc(MEDIUM_BLOCK_SIZE);
    debug_heap(stderr, heap);

    _free(first_block);
    if (!second_block) return BAD_FREE_ONE_BLOCK;
    _free(second_block);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    return OK;
}

enum TEST_RESULT two_blocks_free_test() {
    void* heap = heap_init(REGION_MIN_SIZE);

    debug_heap(stderr, heap);
    void* first_block = _malloc(MEDIUM_BLOCK_SIZE);
    void* second_block = _malloc(MEDIUM_BLOCK_SIZE);
    void* third_block = _malloc(DOUBLE_MEDIUM_BLOCK_SIZE);
    debug_heap(stderr, heap);

    _free(first_block);
    if (!second_block) return BAD_FREE_FIRST_BLOCK;
    _free(second_block);
    if (!third_block) return BAD_FREE_SECOND_BLOCK;
    _free(third_block);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    return OK;
}

enum TEST_RESULT out_of_memory_alloc_after_test() {
    void* heap = heap_init(REGION_MIN_SIZE);

    void* first_block = _malloc(BIG_BLOCK_SIZE_2);
    debug_heap(stderr, heap);

    if (first_block != HEAP_START + offsetof(struct block_header, contents))
        return BAD_ALLOCATE_REGION_AFTER;

    _free(first_block);

    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE * 2}).bytes);
    
    return OK;
}

enum TEST_RESULT out_of_memory_alloc_any_test() {

    void *heap = heap_init(capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes);

    debug_heap(stderr, heap);

    (void) mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void *first_block = _malloc(BIG_BLOCK_SIZE_1);

    if (!first_block) 
        return BAD_ALLOCATE_REGION_ANY;

    debug_heap(stderr, heap);

    return OK;
}

allocator_test tests[] = {
    common_allocate_test,
    one_block_free_test,
    two_blocks_free_test,
    out_of_memory_alloc_after_test,
    out_of_memory_alloc_any_test
};

char* tests_name[] = {
    "allocate one region test",
    "free one block from region test",
    "free two blocks from region test",
    "allocate new region after previous test",
    "allocate new region at random place test"
};

void print_invitation() {
    printf("---------------Tests start---------------\n");
}

void print_test_name(size_t number) {
    int cnt = SEPARATOR_LENGTH - strlen(tests_name[number]);
    for (size_t i = 0; i < cnt / 2 + cnt % 2; i++) printf("-");
    fprintf(stderr, "%s", tests_name[number]);
    for (size_t i = 0; i < cnt / 2; i++) printf("-");
    fprintf(stderr, "\n");
}

void print_test_result(enum TEST_RESULT test_result) {
    char* message = "";
    switch (test_result) {
        case OK:
            message = "test passed";
            break;
        case BAD_ALLOCATE_ONE_BLOCK:
            message = "test failed, bad allocate";
            break;
        case BAD_FREE_ONE_BLOCK:
            message = "test failed, bad free";
            break;
        case BAD_FREE_FIRST_BLOCK:
            message = "test failed, bad free on first block";
            break;
        case BAD_FREE_SECOND_BLOCK:
            message = "test failed, bad free on second block";
            break;
        case BAD_ALLOCATE_REGION_AFTER:
            message = "test failed, bad allocate region after";
            break;
        case BAD_ALLOCATE_REGION_ANY:
            message = "test failed, bad allocate region any";
            break;
    }
    fprintf(stderr, "---------%s---------\n", message);
}

void print_separator() {
    fprintf(stderr, "-----------------------------------------\n");
}

size_t launch_all_tests_and_count() {
    size_t passed_test_counter = 0;
    print_invitation();
    print_separator();
    
    for (size_t test_counter = 0; test_counter < TESTS_COUNT; test_counter++) {
        print_test_name(test_counter);
        enum TEST_RESULT test_result = tests[test_counter]();
        passed_test_counter += test_result == OK ? 1 : 0;
        print_test_result(test_result);
        print_separator();
    }
    return passed_test_counter;
}

