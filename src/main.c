#include "tests.h"


int main ()
{
    size_t passed_tests = launch_all_tests_and_count();
    fprintf(stderr, "tests passed: %zu of %d", passed_tests, TESTS_COUNT);
    return TESTS_COUNT - passed_tests;
}
