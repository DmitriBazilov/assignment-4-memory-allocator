#ifndef TESTS_H
#define TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include <inttypes.h>

#define TESTS_COUNT 5

enum TEST_RESULT {
    OK,
    BAD_ALLOCATE_ONE_BLOCK,
    BAD_FREE_ONE_BLOCK,
    BAD_FREE_FIRST_BLOCK,
    BAD_FREE_SECOND_BLOCK,
    BAD_ALLOCATE_REGION_AFTER,
    BAD_ALLOCATE_REGION_ANY
};

typedef enum TEST_RESULT (*allocator_test)();

size_t launch_all_tests_and_count();

#endif // !TESTS_H
